import React, { Component } from 'react';
import '../../App.css';
import Teste from '../reuse/Teste'
import Header from '../reuse/Header'
import Projects from '../reuse/Projects'
import Services from '../reuse/Services'
import Start from '../reuse/Start'
import Footer from '../reuse/Footer'
import Banner from '../reuse/Banner'

import './capabilities.css';

class Capabilities extends Component {
  render() {
    return (
        <div className="App">
       <Header />
        <Banner title="Capabilities"/>
            <div className="capa">
                <h2>About our Services</h2>
                <p>
                    we bridge the gap between Designers and Clients by 
                    creating strategies that solve design problems to help them
                    realize their goods and connect to the right audience
                </p>
                <hr/>
            </div>
       <Services />
       <Teste />                                                                                                                      
       <Footer />
        </div>
    );
  }
}

export default Capabilities;
