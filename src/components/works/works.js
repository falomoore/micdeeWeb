import React, { Component } from 'react';
import Start from '../reuse/Start';
import Footer from '../reuse/Footer';
import Header from '../reuse/Header';
import Projects from '../reuse/Projects';
import Banner from '../reuse/Banner';
import alma from '../../assets/works/alma.jpg';
import stanbic from '../../assets/works/stanbic.jpg';
import gray from '../../assets/works/gray.jpg';
import canaan from '../../assets/works/canaan.jpg';
import villa from '../../assets/works/villa.jpg';
import country from '../../assets/works/country.jpg';

import '../../App.css';
import './works.css'

class Works extends Component {
  render() {
    return (
      <div className="works">
          <Header />
          <h2>Recent <span>Works</span></h2>
          <div class="work_display" style={{width: "88%", margin: "10% auto"}}>
                <div class="left_work" style={{width: "50%"}}>
                    <div class="work" style={{height: "24em",background: `url(${alma})`}}>
                        <div>Alma Beach</div>
                    </div>
                    <div class="work" style={{height: "26em",background: `url(${canaan})`,marginTop: "25%"}}>
                        <div>Canaan City</div>
                    </div>
                    <div class="work" style={{height: "24em",background: `url(${stanbic})`,marginTop: "37%"}}>
                        <div>Stanbic City</div>
                    </div>
                </div>
                <div class="right_work" style={{width: "50%",marginLeft: "3%"}}>
                    <div class="work" style={{height: "36em",background: `url(${country})`}}>
                        <div>Country Home</div>
                    </div>
                    <div class="work" style={{height: "24em",background: `url(${gray})`,marginTop: "28%"}}>
                        <div>Gray</div>
                    </div>
                    <div class="work" style={{height: "24em",background: `url(${villa})`,marginTop: "28%"}}>
                        <div>Villa L7</div>
                    </div>
                </div>
         </div>
          <button className="view_more">View More</button>
          <Projects />
          <Start />
          <Footer /> 
      </div>
    );
  }
}

export default Works;
