import React, { Component } from 'react';

import '../../App.css';

class Start extends Component {
  render() {
    return (
      <div className="App">
        <div className="comp6">
            <span>Loooking to create a new space?</span> 
            <button>Start a project</button>
        </div>
      </div>
    );
  }
}

export default Start;
