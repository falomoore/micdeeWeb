import React, { Component } from 'react';

import logos from '../../assets/logo.svg'
import '../../App.css';

class Footer extends Component {
  render() {
    return (
      <div className="App">
        <div className="comp7">
            <div className="left_content">
                <div className="logo"><img src={logos}/></div>
                <div className="contact">
                    A: Yabatech GRA, Ikorodu, Lagos<br/>
                    P: +234-8178869191 | +234-8105039737<br/>
                    E: Hello@micdee-designs.com
                </div>
                <div className="social">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
            <div className="right_content">
                <div>
                    <div>STUDIO</div>
                    <div>CAPABILITIES</div>
                    <div>WORK</div>
                    <div>CONTACT</div>
                </div>
                <div>
                    2017, Micdee Designs, All rights reserved
                </div>
            </div>
        </div>
      </div>
    );
  }
}

export default Footer;
