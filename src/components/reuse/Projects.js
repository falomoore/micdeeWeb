import React, { Component } from 'react';
import alma from '../../assets/studio/alma.jpg';
import stanbic from '../../assets/studio/stanbic.jpg';
import '../../App.css';

class Projects extends Component {
  render() {
    return (
      <div className="App">
          <div className="comp5">
             <div className="view">View Projects</div>
             <div className="city">
               <img src={alma}/>
               <span>Alma Beach</span>
             </div>
             <div className="city">
               <img src={stanbic}/>
               <span>Stanbic City</span>
             </div>
             
          </div>
      </div>
    );
  }
}

export default Projects;
