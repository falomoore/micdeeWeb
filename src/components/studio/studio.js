import React, { Component } from 'react';
import Start from '../reuse/Start';
import Footer from '../reuse/Footer';
import Header from '../reuse/Header';
import Projects from '../reuse/Projects';
import Banner from '../reuse/Banner';
import helmet from '../../assets/approach/helmet.svg';
import draw from '../../assets/approach/draw.svg';
import pages from '../../assets/approach/pages.svg';
import talk from '../../assets/approach/talk.svg';
import pen from '../../assets/approach/pen.svg';
import collab from '../../assets/approach/collab.svg';



import '../../App.css';
import './studio.css' 

class Studio extends Component {
  render() {
    return (
      <div className="App">
          <Header />
          <Banner title="Studio"/>
          <div className="culture">
            <div className="about_cult">
                <h2>Who we are</h2>
                <p>
                    Micdee Designs s a young, budding design and visualisation agency
                    .We are innovative, collaborative and use our collective desing and technical expertise to produce excellent designs and visuals for clients and 
                    professionals.
                </p>
                <p>
                    With our vibrant and forward thinking team, we can take on any project ranging from a small interior render for an interiror designer to a more 
                    complex project such as estate designs and visualisations for real estate developers.
                </p>
                <p>
                    With our expertise, we make your project come alive right before your 
                    very eyes, ffering you a first-hand look and analysis of your project.
                </p>
            </div>
            <div classame="buss_cult">
                <h2>Business Culture</h2>
                <p>
                    Micdee Designs is a young, budding design and visualisation
                    agency. We are innovative, collaborative and use our collective 
                    design and technical expertise to produce excellent designs and visuals
                    for clients and professionals.
                </p>
                <p>
                    With our vibrant and forward thinking team, we can take on any
                    project ranging from a small interior render for an interior
                    designer to a more complex project such as estate designs and 
                    visualisation for real estate developers.
                </p>
                <p>
                    With our expertise, we make yur project cme alive right before
                    your very eyes, offering yu a first-hand look and analysis of your 
                    project.
                </p>

            </div>
          </div>
          <div className="approach">
            <div className="left_app">
                <h2>Our Approach</h2>
                <div className="apps">
                    <Approach img={talk} 
                              text="First Contact and consultation with client (Generate brief)"/>
                    <Approach img={collab}
                              text="Communicate brief to client (To ensure all expectations of client are covered in brief)"/>
                    <Approach img={draw}
                              text="Commence design process, ideate, generate concept sketches"/>
                    <Approach img={pages}
                              text="Send first draft to client, Request fo corrections and modifications"/>
                    <Approach img={pen}
                              text="Finalize and issue designs, visuals, drawings and presentations to client"/>
                    <Approach img={helmet}
                              text="Supervise implementation on project on site(if required by project)."/>
                </div>
            </div>
            <div className="right_app">
                <span>Our approach</span>
            </div>
          </div>
          <div className="team_section">
              <h2>Meet the Team</h2>
              <div className="teams">

                <Profile name="Michael Awonowo" occ="Founder/Lead Designer"/>
                <Profile name="Olumide Yomi-Omolayo" occ="Chief of Operations and Media"/>
                <Profile name="Oyelola Oluwatobi" occ="Project Manager & Architect"/>
                <Profile name="Dolapo Fadipe" occ="Architect & 3D Visualiser"/>
              </div>
          </div> 
          <Projects />
           <Start />
          <Footer /> 
      </div>
    );
  }
}
function Approach (props){
    const { img, text} = props;
        return(
            <div className="app">
                 <img src={props.img} />
                 <div>
                    {props.text}
                 </div>
            </div>
        );
}
function Profile (props){
    const { img, name, occ } = props;
        return(
            <div className="team">
                 <div class="team_img"></div>
                 <div class="team_text">
                    {props.name}<br/>
                    {props.occ}
                 </div>
            </div>
        );
}

export default Studio;
