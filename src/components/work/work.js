import React, { Component } from 'react';
import Start from '../reuse/Start';
import Footer from '../reuse/Footer';
import Header from '../reuse/Header';
import Projects from '../reuse/Projects';
import './work.css';
import glove from '../../assets/approach/glove.svg';
import strategy from '../../assets/approach/strategy.svg';
import villa from '../../assets/work/villa.jpg';

class Work extends Component {
  render() {
    return (
      <div className="work">
        <Header />
        <h2>Villa L7</h2>
        <h4>Client -- Private</h4>
        <div className="project"></div>
        <div className="factors">
          <div className="challenge">
            <img src={glove} />
            <h2>Challenge</h2>
            <p>
              How do you create a piece of residential
              architecture that fuses the present and the past
              with the right balance and still achieve functionality
              and aesthetics at the same time?
            </p>
          </div>
          <div className="fac_approach">
            <img src={strategy} />
            <h2>Our Approach</h2>
            <div>
              <p>
                We embark on the project as a collaborative
                effort between our agency and Eada Studios.
              </p>
              <p>
                The aim of the design was to create a proposal
                for a modernist residential model in a remote
                location surrounded by well-developed
                vegetation that beutifully integrates the traditional
                residential spaces into themselves with 
                the ideals of modern architecture's forms and 
                styling.
              </p>
              <p>
                Careful considerations was given to design 
                decisions to achieve an architecture rooted in 
                basic tings such as function, form, structure,
                lighting, materials and details.
              </p>
            </div>
          </div>
        </div>
        <div className="project_pic">
              <div className="left_pic"></div>
              <div className="middle_pic"></div>
              <div className="right_pic"></div>
        </div>
        <Projects/>
        <Start />
        <Footer/>
      </div>
    );
  }
}

export default Work; 
