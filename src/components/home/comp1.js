import React, { Component } from 'react';

import '../../App.css';
import slide1 from '../../assets/slider1.svg'
import logos from '../../assets/logo.svg'
import Header from '../reuse/Header';

class Comp1 extends Component {
    render() {
        return (
            <div className="App">

                <div className="comp1">

                   
                    <Header />
                    <div className="slide">

                        <div className='slideText'>
                            <div style={{ padding: 0, color: 'rgba(255,255,255,0.6)'}}>OUR VALUE PROPOSITION</div>
                            <div style={{fontWeight: 500, paddingTop: 10}}>Bridging the gap between designers and clients </div>

                            <div style={{paddingTop: 15, fontSize: 13}} className='slideSelector'>
                                <div style={{paddingTop: 6, color: 'rgba(255,255,255,0.6)'}}>FROM PROJECT</div>
                                <div style={{backgroundColor: 'white',padding: 6, color:'#9D2B4A', marginLeft: 20,fontWeight:500}}>COUNTRY HOME</div>
                            </div>

                            <div style={{paddingTop: 40}} className='indicators'>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'#9D2B4A', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                                <div style={{width: 90, height: 6, background:'white', borderRadius: 50}}></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

export default Comp1;
